package simulation;


import com.espertech.esper.common.client.configuration.Configuration;
import com.espertech.esper.compiler.client.CompilerArguments;
import com.espertech.esper.compiler.client.EPCompileException;
import com.espertech.esper.compiler.client.EPCompiler;
import com.espertech.esper.compiler.client.EPCompilerProvider;
import com.espertech.esper.runtime.client.EPDeployException;
import com.espertech.esper.runtime.client.EPRuntime;
import com.espertech.esper.runtime.client.EPRuntimeProvider;
import com.espertech.esper.runtime.client.EPStatement;
import com.espertech.esper.runtime.client.util.InputAdapter;
import com.espertech.esperio.csv.AdapterInputSource;
import com.espertech.esperio.csv.CSVInputAdapter;
import com.espertech.esperio.csv.CSVInputAdapterSpec;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * This class allows us to test several patterns using as input the CSV file generated at transformers.ProcessCSV.java.
 * It is important to note that, as here we are telling to the Esper CEP engine to do a 'quick' simulation, the data
 * windows are 'ext_time' instead of 'time', this is because we are not doing a real world simulation spacing the events
 * in the time, but we are feeding all of them at the same time, so we have to tell Esper which is the timestamp column
 * so it is able to calculate the correct data windows.
 * <p>
 * Also, not that the timestamp columns of these CSV are express in milliseconds, not in seconds. So 25.10 in the original
 * file would be 25100 in this file. This is because Esper has to work with the timestamp in milliseconds not in seconds.
 *
 * @author David Corral-Plaza <david.corral@uca.es>
 */

public class Simulation {

    static public Logger logger = Logger.getLogger(Simulation.class);

    public static void main(String[] args) {
        Map<String, Object> eventProperties = new HashMap<>();
        eventProperties.put("p1", String.class);
        eventProperties.put("p2", Integer.class);
        eventProperties.put("p3", double.class);
        eventProperties.put("p4", double.class);
        eventProperties.put("p5", double.class);
        eventProperties.put("p6", double.class);
        eventProperties.put("p7", double.class);
        eventProperties.put("p8", double.class);
        eventProperties.put("p9", double.class);
        eventProperties.put("p10", double.class);
        eventProperties.put("timestamp", long.class);

        Configuration configuration = new Configuration();
        configuration.getCommon().addEventType("HumanActivity", eventProperties);

        EPCompiler compiler = EPCompilerProvider.getCompiler();

        EPRuntime runtime = EPRuntimeProvider.getDefaultRuntime(configuration);

        CompilerArguments arguments = new CompilerArguments(runtime.getRuntimePath());
        arguments.setConfiguration(configuration);

        /*
         * These are the event patterns to be detected in the simulation
         */
        String[] epls = new String[]{
                "@public @buseventtype @Eventrepresentation(json) @Name('StandingOrSitting') INSERT INTO StandingOrSitting SELECT 'Standing or Sitting' as position, a1.timestamp as a1p11, count(a1), count(a2),count(a3),count(a4) FROM pattern [every ( ((a1= HumanActivity(a1.p2 = 1 and a1.p4 between 0.75 and 1.25)) and (a2 = HumanActivity(a2.p2=2 and a2.p4 between 0.75 and 1.25)) and (a3 = HumanActivity(a3.p2=3 and a3.p4 between 0.75 and 1.25)) and (a4 = HumanActivity(a4.p2=4 and a4.p4 between 0.75 and 1.25))))]#ext_timed(a1.timestamp,200 msec)",
                "@public @buseventtype @Eventrepresentation(json) @Name('LyingOnStomach') INSERT INTO LyingOnStomach SELECT 'Lying on Stomach' as position, a1.timestamp as a1p11, count(a1), count(a2),count(a3),count(a4) FROM pattern [(every ((a1= HumanActivity(a1.p2 = 1 and a1.p6 between -1.25 and -0.75)) and (a2 = HumanActivity(a2.p2=2 and a2.p5 between 0.75 and 1.25)) and (a3 = HumanActivity(a3.p2=3 and a3.p6 between 0.75 and 1.25)) and (a4 = HumanActivity(a4.p2=4 and a4.p5 between -1.25 and -0.75))))]#ext_timed(a1.timestamp,200 msec)",
                "@public @buseventtype @Eventrepresentation(json) @Name('LyingOnBack') INSERT INTO LyingOnBack SELECT 'Lying on Back' as position, a1.timestamp as a1p11, count(a1), count(a2),count(a3),count(a4) FROM pattern[(every ((a1= HumanActivity(a1.p2 = 1 and a1.p6 between 0.75 and 1.25)) and(a2 = HumanActivity(a2.p2=2 and a2.p5 between -1.25 and -0.75)) and(a3 = HumanActivity(a3.p2=3 and a3.p6 between -1.25 and -0.75)) and(a4 = HumanActivity(a4.p2=4 and a4.p5 between 0.75 and 1.25))))]#ext_timed(a1.timestamp,200 msec)",
                "@public @buseventtype @Eventrepresentation(json) @Name('LyingOnLeft') INSERT INTO LyingOnLeft SELECT 'Lying on Left side' as position, a1.timestamp as a1p11, count(a1), count(a2),count(a3),count(a4) FROM pattern[(every ((a1= HumanActivity(a1.p2 = 1 and a1.p5 between 0.75 and 1.25)) and(a2 = HumanActivity(a2.p2=2 and a2.p6 between 0.75 and 1.25)) and(a3 = HumanActivity(a3.p2=3 and a3.p5 between -1.25 and -0.75)) and(a4 = HumanActivity(a4.p2=4 and a4.p6 between -1.25 and -0.75))))]#ext_timed(a1.timestamp,200 msec)",
                "@public @buseventtype @Eventrepresentation(json) @Name('LyingOnRight') INSERT INTO LyingOnRight SELECT 'Lying on Right side' as position, a1.timestamp as a1p11, count(a1), count(a2),count(a3),count(a4) FROM pattern[(every ((a1= HumanActivity(a1.p2 = 1 and a1.p5 between -1.25 and -0.75)) and(a2 = HumanActivity(a2.p2=2 and a2.p6 between -1.25 and -0.75)) and(a3 = HumanActivity(a3.p2=3 and a3.p5 between 0.75 and 1.25)) and(a4 = HumanActivity(a4.p2=4 and a4.p6 between 0.75 and 1.25))))]#ext_timed(a1.timestamp,200 msec)",
                "@public @buseventtype @Eventrepresentation(json) @Name('Critical') INSERT INTO Critical Select 'Critical' as position, a1.p2, a2.p2, a2.p3, a1.p3, count(a1), count(a2) FROM pattern[every a1 = HumanActivity -> a2 = HumanActivity (a2.p2 = a1.p2 AND a2.p3 - a1.p3 <= -0.85 )]",
                "@public @buseventtype @Eventrepresentation(json) @Name('Impact') INSERT INTO Impact SELECT 'Impact' as position, a1.p2 as sensorId, count(a1), count(a2) FROM HumanActivity#ext_timed(timestamp,200 msec) as a1, HumanActivity#ext_timed(timestamp,200 msec) as a2 WHERE (a1.p2 = a2.p2) AND (a2.timestamp > a1.timestamp) AND (a2.p3 - a1.p3 >= 0.85)",
                "@Eventrepresentation(json) @Name('FallStomach') INSERT INTO FallStomach SELECT 'Fall detected on stomach' as fallType FROM pattern [every(StandingOrSitting -> Critical -> Impact -> [200]LyingOnStomach)]",
                "@Eventrepresentation(json) @Name('FallBack') INSERT INTO FallBack SELECT 'Fall detected on back' as fallType FROM pattern [every(StandingOrSitting -> Critical -> Impact -> [200]LyingOnBack)]",
                "@Eventrepresentation(json) @Name('FallRight') INSERT INTO FallRight SELECT 'Fall detected on right' as fallType FROM pattern [every(StandingOrSitting -> Critical -> Impact -> [200]LyingOnRight)]",
                "@Eventrepresentation(json) @Name('FallLeft') INSERT INTO FallLeft SELECT 'Fall detected on left' as fallType FROM pattern [every(StandingOrSitting -> Critical -> Impact -> [200]LyingOnLeft)]"};
        HashMap<String, Integer> complexEventsDetected = new HashMap<>();

        try {
            for (int i = 0; i < epls.length; i++) {
                String epl = epls[i];
                logger.info("====> Processing EPL nº" + i);
                EPStatement stmt = compileAndDeploy(runtime, compiler, arguments, epl);
                arguments = new CompilerArguments(runtime.getRuntimePath());
                arguments.setConfiguration(configuration);

                stmt.addListener((newComplexEvents, oldComplexEvents, detectedEventPattern, epRuntime) -> {
                    if (newComplexEvents != null) {
                        String eventPatternName = detectedEventPattern.getEventType().getName();
                        complexEventsDetected.put(eventPatternName,
                                complexEventsDetected.get(eventPatternName) == null ? 1 : complexEventsDetected.get(eventPatternName) + 1);
                        logger.info("** Complex event '" + eventPatternName + "' detected: " + newComplexEvents[0].getUnderlying());
                    }
                });
            }
        } catch (EPCompileException e) {
            logger.error("Problem compiling the EPL");
            e.printStackTrace();
        } catch (EPDeployException e) {
            logger.error("Problem deploying the EPL");
            e.printStackTrace();
        }

        /*
         * Here we specify the CSV file to be used as input
         */
        CSVInputAdapterSpec spec = new CSVInputAdapterSpec(
                new AdapterInputSource("april-luigi/4nodes/april-luigi-4nodes-frontfall.csv"),
                "HumanActivity");
        spec.setTimestampColumn("timestamp");
        spec.setUsingExternalTimer(true);
        logger.info(spec.getTimestampColumn());

        InputAdapter inputAdapter = new CSVInputAdapter(runtime, spec);
        inputAdapter.start();

        for (Map.Entry<String, Integer> entry : complexEventsDetected.entrySet()) {
            logger.info(entry.getKey() + ":" + entry.getValue());
        }
    }

    public static EPStatement compileAndDeploy(EPRuntime runtime, EPCompiler compiler, CompilerArguments arguments, String EPL)
            throws EPCompileException, EPDeployException {
        return runtime.getDeploymentService().deploy(compiler.compile(EPL, arguments)).getStatements()[0];
    }
}
