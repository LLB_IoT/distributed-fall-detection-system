package utils;

import java.util.Comparator;

public class HumanActivity {
    private String p1;
    private int p2;
    private double p3, p4, p5, p6, p7, p8, p9, p10;
    private long p11;

    public HumanActivity(int p2, double p3, double p4, double p5, double p6, double p7, double p8, double p9, double p10, long p11) {
        this.p1 = "HumanActivity";
        this.p2 = p2;
        this.p3 = p3;
        this.p4 = p4;
        this.p5 = p5;
        this.p6 = p6;
        this.p7 = p7;
        this.p8 = p8;
        this.p9 = p9;
        this.p10 = p10;
        this.p11 = p11;
    }

    public String getP1() {
        return p1;
    }

    public void setP1(String p1) {
        this.p1 = p1;
    }

    public int getP2() {
        return p2;
    }

    public void setP2(int p2) {
        this.p2 = p2;
    }

    public double getP3() {
        return p3;
    }

    public void setP3(double p3) {
        this.p3 = p3;
    }

    public double getP4() {
        return p4;
    }

    public void setP4(double p4) {
        this.p4 = p4;
    }

    public double getP5() {
        return p5;
    }

    public void setP5(double p5) {
        this.p5 = p5;
    }

    public double getP6() {
        return p6;
    }

    public void setP6(double p6) {
        this.p6 = p6;
    }

    public double getP7() {
        return p7;
    }

    public void setP7(double p7) {
        this.p7 = p7;
    }

    public double getP8() {
        return p8;
    }

    public void setP8(double p8) {
        this.p8 = p8;
    }

    public double getP9() {
        return p9;
    }

    public void setP9(double p9) {
        this.p9 = p9;
    }

    public double getP10() {
        return p10;
    }

    public void setP10(double p10) {
        this.p10 = p10;
    }

    public long getP11() {
        return p11;
    }

    public void setP11(long p11) {
        this.p11 = p11;
    }

    public String[] getObject() {
        return new String[]{getP1(), String.valueOf(getP2()), String.valueOf(getP3()), String.valueOf(getP4()),
                String.valueOf(getP5()), String.valueOf(getP6()), String.valueOf(getP7()), String.valueOf(getP8()),
                String.valueOf(getP9()), String.valueOf(getP10()), String.valueOf(getP11())};
    }

    public static class OrderByTimestamp implements Comparator<HumanActivity> {
        public int compare(HumanActivity o1, HumanActivity o2) {
            return Double.compare(o1.getP11(), o2.getP11());
        }
    }

}

