# Fall Detection

## Structure

In this repo, you can find a Java/Maven project which allows you to run offline simulations with data regarding falls
that we have previously collected. The structure is as follows:

- `assets` contain the data collected from the sensor nodes. This data is used in the Maven project to perform the
  simulations.
- `src` contains the whole project with Java classes. To use it, please keep reading.
- `pom.xml` is the maven project description file.

## How to use this project

In order to use this project, follow these instructions:

1. Clone the repo
2. Open this Java/Maven project in your favorite IDE (IntelliJ IDEA, Eclipse, VisualStudio, etc.)
3. Once you have set up the repo, now you can process CSV files or do some simulations.
    - To process CSV files, use the class `ProcessCSV.java`:
        - This class is in the `transformers` package. Edit the class specifying the CSV to be used as input. Usually,
          these CSV files are stored at the `/assets` folder.
    - To simulate an execution, use the class `Simulation.java`
        - This class is in the `simulation` package. Edit the class specifying the patterns to be detected and the CSV
          file to be used as input. Usually, the input CSV files are stored in the `/resources` folder.
